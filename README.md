# audience-lighthouse

## Install Command

```bash
npm install
```

## Run Command

### Benchmark on a single page

```bash
URL=<page URL with prefix> npm run light
```

multiple URLs are accepted too, just split them with comma, such as `<URL 1>,<URL 2>,<URL 3>`

### Benchmark on Homepage/Index Page/Article Page

```bash
SCOPE=<scope> REP=<rep> ENV=<env> npm run benchmark
```

#### `scope`:

The scope of benchmark

`H`: include `H` to benchmark against Homepage of Canberra Times and New Castle Heralds.

`I`: include `I` to benchmark against all the Index Pages of Canberra Times and New Castle Heralds.

`A`: include `A` to grab all the articles on the Index Pages of Canberra Times and New Castle Heralds and benchmark them.

The default value for `SCOPE` is `HIA`


#### `rep`:

Repetition of each page, maximum 9

The default value for `REP` is 9

#### `env`:

Environment of the sites, can be set as `PROD` or `UAT`

Article pages will NOT be benchmarked in UAT environment as there are not enough pages

#### Examples

```bash
SCOPE=HIA REP=5 npm run benchmark
```

```bash
SCOPE=HA npm run benchmark
```

```bash
SCOPE=I ENV=UAT npm run benchmark
```

```bash
REP=1 npm run benchmark
```
