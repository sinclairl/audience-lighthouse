const {Storage} = require('@google-cloud/storage')
const path = require('path');
const fs = require('fs');


module.exports = {
    uploadToGCS: function(filePath) {
        const storage = new Storage({
            keyFilename: path.join(__dirname, '../credentials/auth.json'),
            projectId: 'acm-data-warehouse'
        });
        const bucket = storage.bucket('audience_site_performance');
        bucket.upload((filePath), (err, file) => {
            if (err)
                {
                    console.log(err)
                    throw new Error(err);
                }
        });

    }
}