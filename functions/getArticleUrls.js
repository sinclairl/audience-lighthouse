const puppeteer = require('puppeteer');
const fs = require('fs');

const indexPage = require('../sites/index.json');

exports.getArticleUrls = async () => {
    let urlResult = []

    const browser = await puppeteer.launch({
        headless: true,
        slowMo: 0,
        devtools: false
    })
    const page = await browser.newPage();

    for (let index = 0; index < indexPage.length; index++) {
        try {
            let attr
            let result = {
                "url": [],
                "publication": indexPage[index].publication,
                "category": indexPage[index].category
            }
            if(indexPage[index].category === 'news'){
                await page.goto(indexPage[index].url);
                await page.waitForSelector("a[class='paywall-login']", { visible: true })
                await page.click("a[class='paywall-login']")
                await page.waitForSelector('.tp-modal', { visible: true })
                let iframeUrl = await page.$eval("div[class='tp-modal']  > div > iframe", el => el.getAttribute("src"))
                await page.goto(iframeUrl)
                const button = await page.waitForSelector(".pn-login__button", { visible: true })
                await page.type('#email', 'barryallencips@gmail.com')
                await page.waitFor(1000)
                await page.type('#password', 'baLighthouse')
                await page.click(".pn-login__button")
                await page.waitFor(1000)            
            }

            await page.goto(indexPage[index].url);
            let urlPrefix = indexPage[index].publication === "CT" ? "https://www.canberratimes.com.au" : "https://www.newcastleherald.com.au";
            urlPrefix = indexPage[index].category === "digital-print-edition" ? "" : urlPrefix

            if(indexPage[index].category === "digital-print-edition"){
                attr = await page.$$eval("#dpe-list > div:nth-of-type(1) > a", el => el.map(x => x.getAttribute("href")));
            } else if (indexPage[index].category === "classifieds"){
                attr = await page.$$eval("#classifieds-nav > div > div > ul > li > a", el => el.map(x => x.getAttribute("href")));
            } else {
                //console.log(indexPage[index].category)
                attr = await page.$$eval("*[class*='story__lead'] > a", el => el.map(x => x.getAttribute("href")));
                //console.log(attr)
            }

            for (let i = 0; result.url.length < 9; i++) {
                if(!result.url.includes(urlPrefix + attr[i])){
                    result.url.push(urlPrefix + attr[i])
                }
            }
            //console.log(result)
            urlResult.push(result)
            //console.log(urlResult)
        } catch (error) {
            console.log(error)
        }
    }

    await browser.close();
    const jsonResult = JSON.stringify(urlResult);
    fs.writeFileSync('./sites/articles.json', jsonResult)
}

