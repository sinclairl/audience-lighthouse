const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

const launchChromeAndRunLighthouse = (url, opts, config = null) => {
    return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
            opts.port = chrome.port;
            return lighthouse(url, opts, config).then(results => {
            return chrome.kill().then(() => results.lhr)
        });
    });
}

const median = (values) => {
    if(values.length === 0) return 0;

    values.sort(function(a,b){
        return a-b;
    });

    let half = Math.floor(values.length / 2);

    if (values.length % 2)
        return values[half];

    return (values[half - 1] + values[half]) / 2.0;
}

const chromeArgs = {
    chromeFlags: [
        '--show-paint-rects',
        '--headless',
        "--disable-extensions",
        "--no-first-run",
        "--enable-automation"
    ]
};

module.exports = {
    getLightHouseResult: async function(sites, rep, page) {
        let publication = sites.publication
        let category = sites.category
        let url = sites.url;

        let result = {
            "publication": publication,
            "category": category,
            "lhResult": {
                "performance": [],
                "accessibility": [],
                "best-practices": [],
                "seo": [],
                "pwa": []
            },
            "lhMedian": {
                "performance": [],
                "accessibility": [],
                "best-practices": [],
                "seo": [],
                "pwa": []
            }
        }

        for (let i = 0; i < rep; i++) {
            try {
                let res
                if (page === 'articlePage') {
                    res = await launchChromeAndRunLighthouse(url[i], chromeArgs);
                } else {
                    res = await launchChromeAndRunLighthouse(url, chromeArgs);
                }
                
                if (res) {
                    let results = Object.keys(res.categories).reduce((sum, category) => {
                        sum[category] = res.categories[category].score
                        return sum
                    }, {})
                    result.lhResult["performance"].push(results.performance)
                    result.lhResult["accessibility"].push(results.accessibility)
                    result.lhResult["best-practices"].push(results["best-practices"])
                    result.lhResult["seo"].push(results.seo)
                    result.lhResult["pwa"].push(results.pwa)
                }
            } catch (error) {
                console.log(error);
            }
        }
        result.lhMedian["performance"] = median(result.lhResult["performance"])
        result.lhMedian["accessibility"] = median(result.lhResult["accessibility"])
        result.lhMedian["best-practices"] = median(result.lhResult["best-practices"])
        result.lhMedian["seo"] = median(result.lhResult["seo"])
        result.lhMedian["pwa"] = median(result.lhResult["pwa"])
        //result.lhMedian["pwa"].push(median(result.lhResult["pwa"]))
        //console.log(result)
        return result
        //return JSON.stringify(result);
    }

}