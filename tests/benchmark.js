const getArticleUrls = require('../functions/getArticleUrls');
const lighthouse = require('../functions/lighthouse');
const gcsUpload = require('../functions/googleCloudUpload')
const fs = require('fs');
const indexPage = require('../sites/index.json');
const homePage = require('../sites/home.json');
const indexPageUAT = require('../sites/indexUAT.json');
const homePageUAT = require('../sites/homeUAT.json');
const moment = require('moment');
const path = require('path');

const scope = process.env.SCOPE;
const rep = process.env.REP;
const env = process.env.ENV;

const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const benchmark = async (scope = 'HIA', env= 'PROD', rep = 9) => {
    let now

    //Delete all the JSON files in results folder before running the scripts
    //let fileList = fs.readdirSync(path.join(__dirname, '../results'))

    if(env === 'UAT'){
        scope = scope.replace('A','')
    }

    // for (const fileName of fileList) {
    //     if(fileName.split('.')[1] !== 'gitkeep'){
    //         fs.unlinkSync(path.join(__dirname, `../results/${fileName}`))
    //     }
    // }

    //benchmark homepage

    if(scope.includes('H')){
        let homeResult = [];
        let URLs = (env === 'UAT' ? homePageUAT : homePage)
        console.log('**************BENCHMARKING HOME PAGE...*****************')
        await asyncForEach(URLs, async (site) => {
            let siteResult = await lighthouse.getLightHouseResult(site, rep, 'homePage')
            homeResult.push(siteResult)
        })
        now = moment().format('HH:mm DD-MM-YYYY')
        let homeJSON = JSON.stringify(homeResult)
        let filePath = path.join(__dirname, `../results/Homepage ${env} ${now}.json`)
        fs.writeFileSync(filePath, homeJSON)
        console.log('************HOME PAGE BENCHMARKING FINISHED*************')
        gcsUpload.uploadToGCS(filePath)
        await sleep(5000)
        await fs.unlink(filePath, (err) => {
            if (err) throw err;
        });
    }
    
    //benchmark index page

    if(scope.includes('I')){
        let indexResult = [];
        let URLs = (env === 'UAT' ? indexPageUAT : indexPage)
        console.log('**************BENCHMARKING INDEX PAGE...****************')
        await asyncForEach(URLs, async (site) => {
            // console.log(site.url)
            let siteResult = await lighthouse.getLightHouseResult(site, rep, 'indexPage')
            indexResult.push(siteResult)
        })
        now = moment().format('HH:mm DD-MM-YYYY')
        let indexJSON = JSON.stringify(indexResult)
        let filePath = path.join(__dirname, `../results/IndexPage ${env} ${now}.json`)
        fs.writeFileSync(filePath, indexJSON)
        console.log('***********INDEX PAGE BENCHMARKING FINISHED*************')
        gcsUpload.uploadToGCS(filePath)
        await sleep(5000)
        await fs.unlink(filePath, (err) => {
            if (err) throw err;
        });
    }
    
    //benchmark article page
    
    if(scope.includes('A')){
        console.log('**********EXTRACTING URLS FROM ARTICLE PAGES************')
        await getArticleUrls.getArticleUrls()
        console.log('**************BENCHMARKING ARTICLE PAGE...**************')
        let articlePage = fs.readFileSync('./sites/articles.json');
        articlePage = JSON.parse(articlePage)
        let articleResult = [];
        await asyncForEach(articlePage, async (site) => {
            let siteResult = await lighthouse.getLightHouseResult(site, rep, 'articlePage')
            articleResult.push(siteResult)
        })
        now = moment().format('HH:mm DD-MM-YYYY')
        let articleJSON = JSON.stringify(articleResult)
        let filePath = path.join(__dirname, `../results/ArticlePage ${env} ${now}.json`)
        fs.writeFileSync(filePath, articleJSON)
        console.log('**********ARTICLE PAGE BENCHMARKING FINISHED************')
        gcsUpload.uploadToGCS(filePath)
        await sleep(5000)
        await fs.unlink(filePath, (err) => {
            if (err) throw err;
        });
    }

    console.log('*************ALL DONE**************')
}


benchmark(scope, env, rep)
    //.then(() => process.exit())