const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const url = process.env.URL;


const launchChromeAndRunLighthouse = (url, opts, config = null) => {
    return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
            opts.port = chrome.port;
            return lighthouse(url, opts, config).then(results => {
            // use results.lhr for the JS-consumeable output
            // https://github.com/GoogleChrome/lighthouse/blob/master/types/lhr.d.ts
            // use results.report for the HTML/JSON/CSV output as a string
            // use results.artifacts for the trace/screenshots/other specific case you need (rarer)
            return chrome.kill().then(() => results.lhr)
        });
    });
}

const opts = {
    chromeFlags: [
        '--show-paint-rects',
        '--headless',
        "--disable-extensions",
        "--no-first-run",
        "--enable-automation"
    ]
};

const turnOnTheLight = async (url) => {
    if(url.includes(',')){
        let urlArray = url.split(',')
        let resultsArray = []
        for (let index = 0; index < urlArray.length; index++) {
            let url = urlArray[index];
            let res = await launchChromeAndRunLighthouse(url, opts)
            let results = Object.keys(res.categories).reduce((merged, category) => {
                merged[category] = res.categories[category].score
                return merged
            }, {})
            let resultObject = {
                url,
                score: results
            }
            resultsArray.push(resultObject)
        }
        const jsonResult = JSON.stringify(resultsArray);
        let now = moment().format('HH:mm DD-MM-YYYY');
        let filePath = path.join(__dirname, `./results/SingleRun-${now}.json`)
        fs.writeFileSync(filePath, jsonResult)

    } else {
        let res = await launchChromeAndRunLighthouse(url, opts)
        let results = Object.keys(res.categories).reduce((merged, category) => {
            merged[category] = res.categories[category].score
            return merged
        }, {})
        let resultObject = {
            url,
            score: results
        }
        const jsonResult = JSON.stringify(resultObject);
        let now = moment().format('HH:mm DD-MM-YYYY');
        let filePath = path.join(__dirname, `../results/SingleRun-${now}.json`)
        fs.writeFileSync(filePath, jsonResult)
    };
    console.log('BENCHMARK FINISHED')
    
}

turnOnTheLight(url).then(() => process.exit())